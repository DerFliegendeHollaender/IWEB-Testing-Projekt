import {
  getMovies,
  createMovie,
  updateMovie,
} from "../movie/movie.controller.js";
import { model as Movie } from "../movie/movie.model.js";

jest.mock("../movie/movie.model.js");

// Gemockte Response, welche der Controller übergeben wird
const response = {
  status: jest.fn((x) => x),
  json: jest.fn((x) => x),
};

describe("Getting Movies", () => {
  afterEach(() => {
    jest.resetAllMocks();
  });

  test("Loading Movies successfully", async () => {
    // given - Vorbereiten von erfolgreichem Laden von Angeboten
    //         Mocking von Model
    const storedMovies = [
      {
        _id: "1",
        name: "Avengers: Endgame",
        year: 2019,
        genre: "Action, Adventure, Drama",
        director: "Anthony Russo, Joe Russo",
        actors: "Robert Downey Jr., Chris Evans, Mark Ruffalo, Chris Hemsworth",
        description:
          "After the devastating events of Avengers: Infinity War (2018), the universe is in ruins. With the help of remaining allies, the Avengers assemble once more in order to reverse Thanos' actions and restore balance to the universe.",
        asset: "./../assets/endgame.jpg",
        belongsToMyCollection: true,
      },
      {
        _id: "2",
        name: "Creed II",
        year: 2018,
        genre: "Drama, Sport",
        director: "Steven Caple Jr.",
        actors:
          "Michael B. Jordan, Sylvester Stallone, Tessa Thompson, Phylicia Rashad",
        description:
          "Under the tutelage of Rocky Balboa, newly crowned heavyweight champion Adonis Creed faces off against Viktor Drago, the son of Ivan Drago.",
        asset: "./../assets/creed2.jpg",
        belongsToMyCollection: true,
      },
      {
        _id: "3",
        name: "Grown Ups 2",
        year: 2013,
        genre: "Comedy",
        director: "Dennis Dugan",
        actors: "Adam Sandler, Kevin James, Chris Rock, David Spade",
        description:
          "After moving his family back to his hometown to be with his friends and their kids, Lenny finds out that between old bullies, new bullies, schizo bus drivers, drunk cops on skis, and four hundred costumed party crashers sometimes crazy follows you.",
        asset: "./../assets/grownups2.jpg",
        belongsToMyCollection: true,
      },
    ];
    Movie.find.mockImplementation(() => {
      return {
        exec: jest.fn().mockResolvedValue(storedMovies),
      };
    });

    // when - Angebote laden
    await getMovies(null, response);

    // then - überprüfen, ob korrekte Angebote
    // geladen wurden
    expect(response.json).toHaveBeenCalledWith(storedMovies);
  });
  test("Loading Movies unsuccessfully", async () => {
    // given - Vorbereiten von fehlgeschlagenen Laden von Angeboten
    //         Mocking von Model
    Movie.find.mockImplementation(() => {
      return {
        exec: jest.fn().mockRejectedValue({ message: "DB error" }),
      };
    });

    // when - Angebote laden
    await getMovies(null, response);

    // then - überprüfen, ob richtiger Fehler aufgetreten ist
    expect(response.status).toHaveBeenCalledWith(500);
    expect(response.json).toHaveBeenCalledWith({ message: "DB error" });
  });
});

describe("Creating Movies", () => {
  afterEach(() => {
    jest.resetAllMocks();
  });

  // Gemockter Request, welcher Controller übergeben wird
  const request = {
    body: {
      name: "Avengers: Endgame",
      year: 2019,
      genre: "Action, Adventure, Drama",
      director: "Anthony Russo, Joe Russo",
      actors: "Robert Downey Jr., Chris Evans, Mark Ruffalo, Chris Hemsworth",
      description:
        "After the devastating events of Avengers: Infinity War (2018), the universe is in ruins. With the help of remaining allies, the Avengers assemble once more in order to reverse Thanos' actions and restore balance to the universe.",
      asset: "./../assets/endgame.jpg",
      belongsToMyCollection: true,
    },
  };

  test("Creating Movies successfully", async () => {
    // given - Vorbereiten von erfolgreichem Speichern von Angeboten
    //         Mocking von Model
    const storedMovie = {
      id: "1",
      name: request.body.name,
      year: request.body.year,
      genre: request.body.genre,
      director: request.body.director,
      actors: request.body.actors,
      description: request.body.description,
      asset: request.body.asset,
      belongsToMyCollection: request.body.belongsToMyCollection,
    };
    Movie.create.mockResolvedValue(storedMovie);

    // when - Angebote laden
    await createMovie(request, response);

    // then - überprüfen, ob korrekte Angebote geladen wurden
    expect(response.json).toHaveBeenCalledWith(storedMovie);
  });

  test("Creating Movies unsuccessfully", async () => {
    // given - Vorbereiten von fehlgeschlagenen Speichern von Angeboten
    //         Mocking von Model
    Movie.create.mockRejectedValue({ message: "DB error" });

    // when - Angebote laden
    await createMovie(request, response);

    // then - überprüfen, ob richtiger Fehler aufgetreten ist
    expect(response.status).toHaveBeenCalledWith(500);
    expect(response.json).toHaveBeenCalledWith({ message: "DB error" });
  });
});

describe("Updating Movies", () => {
  afterEach(() => {
    jest.resetAllMocks();
  });

  // Gemockter Request, welcher Controller übergeben wird
  const request = {
    body: {
      id: "1",
      name: "Avengers: Endgame",
      year: 2019,
      genre: "Action, Adventure, Drama",
      director: "Anthony Russo, Joe Russo",
      actors: "Robert Downey Jr., Chris Evans, Mark Ruffalo, Chris Hemsworth",
      description:
        "After the devastating events of Avengers: Infinity War (2018), the universe is in ruins. With the help of remaining allies, the Avengers assemble once more in order to reverse Thanos' actions and restore balance to the universe.",
      asset: "./../assets/endgame.jpg",
      belongsToMyCollection: true,
    },
  };

  test("Updating Movies successfully", async () => {
    // given - Vorbereiten von erfolgreichem Speichern von Angeboten
    //         Mocking von Model
    const movieToUpdate = {
      id: "1",
      name: "Avengers: Endgame",
      year: 2019,
      genre: "Action, Adventure, Drama",
      director: "Anthony Russo, Joe Russo",
      actors: "Robert Downey Jr., Chris Evans, Mark Ruffalo, Chris Hemsworth",
      description:
        "After the devastating events of Avengers: Infinity War (2018), the universe is in ruins. With the help of remaining allies, the Avengers assemble once more in order to reverse Thanos' actions and restore balance to the universe.",
      asset: "./../assets/endgame.jpg",
      belongsToMyCollection: false,
      save: jest.fn().mockResolvedValue({
        id: "1",
        name: request.body.name,
        year: request.body.year,
        genre: request.body.genre,
        director: request.body.director,
        actors: request.body.actors,
        description: request.body.description,
        asset: request.body.asset,
        belongsToMyCollection: request.body.belongsToMyCollection,
      }),
    };

    Movie.findById.mockImplementation(() => {
      return {
        exec: jest.fn().mockResolvedValue(movieToUpdate),
      };
    });

    // when - Angebote laden
    await updateMovie(request, response);

    // then
    // 1. überprüfen, ob geladenes Angebot korrekt angepasst wird
    expect(movieToUpdate.belongsToMyCollection).toBe(
      request.body.belongsToMyCollection
    );
    // 2. überprüfen, ob angepasstes Angebot response hinzugefügt wird
    expect(response.json).toHaveBeenCalledWith({
      id: "1",
      name: request.body.name,
      year: request.body.year,
      genre: request.body.genre,
      director: request.body.director,
      actors: request.body.actors,
      description: request.body.description,
      asset: request.body.asset,
      belongsToMyCollection: request.body.belongsToMyCollection,
    });
  });

  test("Updating Movies fails because id is missing", async () => {
    // given - Vorbereiten von fehlgeschlagenen Speichern von Angeboten
    //         Mocking von Model
    Movie.findById.mockImplementation((id) => {
      return {
        exec: jest.fn().mockResolvedValue(null),
      };
    });

    // when - Angebote laden
    await updateMovie(request, response);

    // then - überprüfen, ob richtiger Fehler aufgetreten ist
    expect(response.status).toHaveBeenCalledWith(404);
    expect(response.json).toHaveBeenCalledWith({
      message: "Movie with id 1 not found.",
    });
  });

  test("Updating Movies fails because of a db error", async () => {
    // given - Vorbereiten von fehlgeschlagenen Speichern von Angeboten
    //         Mocking von Model
    Movie.findById.mockImplementation((id) => {
      return {
        exec: jest.fn().mockRejectedValue({ message: "DB error" }),
      };
    });

    // when - Angebote laden
    await updateMovie(request, response);

    // then - überprüfen, ob richtiger Fehler aufgetreten ist
    expect(response.status).toHaveBeenCalledWith(500);
    expect(response.json).toHaveBeenCalledWith({ message: "DB error" });
  });
});
