import { model as Movie } from "./movie.model.js";

async function getMovies(request, response) {
  await Movie.find()
    .exec()
    .then((movies) => {
      response.json(movies);
    })
    .catch((error) => {
      response.status(500);
      response.json({ message: error.message });
    });
}

async function createMovie(request, response) {
  const newMovie = request.body;
  await Movie.create({
    name: newMovie.name,
    belongsToMyCollection: newMovie.belongsToMyCollection,
    asset: newMovie.asset,
  })
    .then((savedMovie) => {
      response.json(savedMovie);
    })
    .catch((error) => {
      response.status(500);
      response.json({ message: error.message });
    });
}

async function updateMovie(request, response) {
  const movieId = request.body.id;
  await Movie.findById(movieId)
    .exec()
    .then((movie) => {
      if (!movie) {
        return Promise.reject({
          message: `Movie with id ${movieId} not found.`,
          status: 404,
        });
      }
      const providedMovie = request.body;
      movie.name = providedMovie.name;
      movie.belongsToMyCollection = providedMovie.belongsToMyCollection;
      movie.asset = providedMovie.asset;
      return movie.save();
    })
    .then((updatedMovie) => {
      response.json(updatedMovie);
    })
    .catch((error) => {
      if (error.status) {
        response.status(error.status);
      } else {
        response.status(500);
      }
      response.json({ message: error.message });
    });
}

export { getMovies, createMovie, updateMovie };
