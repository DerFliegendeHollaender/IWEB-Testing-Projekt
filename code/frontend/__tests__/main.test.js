/**
 * @jest-environment jsdom
 */

import {
  waitFor,
  waitForElementToBeRemoved,
  getByText,
  getByTitle,
} from "@testing-library/dom";

import * as axiosModule from "../scripts/libs/axios.min.js";
jest.mock("../scripts/libs/axios.min.js");

function setUpDom() {
  document.body.innerHTML = `
    <h1>Movie Collection</h1>
    <div class="buttons">
      <button id="new-movies-btn">New Movies</button>
      <button id="my-movies-btn">My Movies</button>
    </div>
    <div class="search" id="search">
      <input type="text" id="search-input" placeholder="Search..." />
      <button id="search-btn">Search</button>
    </div>
    <div id="popup" class="popup hidden"></div>
    <div id="data"></div>
  `;
}

function prepareLoadOfMovies(loadedMovies, isRejected = false) {
  return initMain(loadedMovies, isRejected);
}

function initMain(loadedMovies, isRejected = false) {
  if (!isRejected) {
    axiosModule.get.mockResolvedValue({ data: loadedMovies });
  } else {
    axiosModule.get.mockRejectedValue();
  }
  global.axios = axiosModule;

  return import("../scripts/main").then(() => {
    if (!isRejected && loadedMovies.length > 0) {
      // Laden von Angeboten (asynchroner Prozess) abwarten
      return waitFor(() =>
        getByText(document.querySelector("#data"), loadedMovies[0].name)
      );
    }
  });
}

describe("setUpDom", () => {
  beforeEach(() => {
    setUpDom();
  });

  afterEach(() => {
    jest.resetModules();
    jest.resetAllMocks();
  });

  function prepareAddOfMovie(addedMovie) {
    axiosModule.post.mockResolvedValue({ data: addedMovie });

    return initMain([]);
  }
  function prepareChangeOfMovie(changedMovie, loadedMovies) {
    axiosModule.put.mockResolvedValue({ data: changedMovie });
    return initMain(loadedMovies);
  }

  function checkMovieList(expectedMovies) {
    const elDivMovies = document.getElementsByClassName("movie");

    // check number of rows in offerings table
    expect(elDivMovies.length).toBe(expectedMovies.length);

    for (let i = 0; i < elDivMovies.length; i++) {
      const elDivMovie = elDivMovies[i];

      const expectedMovie = expectedMovies[i];
      expect(
        elDivMovie.classList.contains(expectedMovie.belongsToMyCollection)
      ).toBe(expectedMovie.belongsToMyCollection);
    }
  }

  test("should correctly set up the DOM", () => {
    expect(document.getElementById("search-btn")).not.toBeNull();
    expect(document.getElementById("new-movies-btn")).not.toBeNull();
    expect(document.getElementById("my-movies-btn")).not.toBeNull();
    expect(document.getElementById("search-input")).not.toBeNull();
    expect(document.getElementById("popup")).not.toBeNull();
    expect(document.getElementById("data")).not.toBeNull();
  });
  test("loading Movies successfully", async () => {
    // given  - Nichts zu tun
    // when - Vorbereiten von Laden von Angeboten
    const loadedMovies = [
      {
        _id: "1",
        name: "Avengers: Endgame",
        year: 2019,
        genre: "Action, Adventure, Drama",
        director: "Anthony Russo, Joe Russo",
        actors: "Robert Downey Jr., Chris Evans, Mark Ruffalo, Chris Hemsworth",
        description:
          "After the devastating events of Avengers: Infinity War (2018), the universe is in ruins. With the help of remaining allies, the Avengers assemble once more in order to reverse Thanos' actions and restore balance to the universe.",
        asset: "./../assets/endgame.jpg",
        belongsToMyCollection: true,
      },
      {
        _id: "2",
        name: "Creed II",
        year: 2018,
        genre: "Drama, Sport",
        director: "Steven Caple Jr.",
        actors:
          "Michael B. Jordan, Sylvester Stallone, Tessa Thompson, Phylicia Rashad",
        description:
          "Under the tutelage of Rocky Balboa, newly crowned heavyweight champion Adonis Creed faces off against Viktor Drago, the son of Ivan Drago.",
        asset: "./../assets/creed2.jpg",
        belongsToMyCollection: true,
      },
      {
        _id: "3",
        name: "Grown Ups 2",
        year: 2013,
        genre: "Comedy",
        director: "Dennis Dugan",
        actors: "Adam Sandler, Kevin James, Chris Rock, David Spade",
        description:
          "After moving his family back to his hometown to be with his friends and their kids, Lenny finds out that between old bullies, new bullies, schizo bus drivers, drunk cops on skis, and four hundred costumed party crashers sometimes crazy follows you.",
        asset: "./../assets/grownups2.jpg",
        belongsToMyCollection: true,
      },
    ];
    await prepareLoadOfMovies(loadedMovies);
    // then
    const expectedMovies = [
      {
        name: "Avengers: Endgame",
        year: 2019,
        genre: "Action, Adventure, Drama",
        director: "Anthony Russo, Joe Russo",
        actors: "Robert Downey Jr., Chris Evans, Mark Ruffalo, Chris Hemsworth",
        description:
          "After the devastating events of Avengers: Infinity War (2018), the universe is in ruins. With the help of remaining allies, the Avengers assemble once more in order to reverse Thanos' actions and restore balance to the universe.",
        asset: "./../assets/endgame.jpg",
        belongsToMyCollection: true,
      },
      {
        name: "Creed II",
        year: 2018,
        genre: "Drama, Sport",
        director: "Steven Caple Jr.",
        actors:
          "Michael B. Jordan, Sylvester Stallone, Tessa Thompson, Phylicia Rashad",
        description:
          "Under the tutelage of Rocky Balboa, newly crowned heavyweight champion Adonis Creed faces off against Viktor Drago, the son of Ivan Drago.",
        asset: "./../assets/creed2.jpg",
        belongsToMyCollection: true,
      },
      {
        name: "Grown Ups 2",
        year: 2013,
        genre: "Comedy",
        director: "Dennis Dugan",
        actors: "Adam Sandler, Kevin James, Chris Rock, David Spade",
        description:
          "After moving his family back to his hometown to be with his friends and their kids, Lenny finds out that between old bullies, new bullies, schizo bus drivers, drunk cops on skis, and four hundred costumed party crashers sometimes crazy follows you.",
        asset: "./../assets/grownups2.jpg",
        belongsToMyCollection: true,
      },
    ];
    checkMovieList(expectedMovies);
  });

  test("adding new movie successfully", async () => {
    // given
    // 1. Vorbereiten von Hinzufügen von Angebot
    const addedMovie = [
      {
        _id: "1",
        name: "Avengers: Endgame",
        year: 2019,
        genre: "Action, Adventure, Drama",
        director: "Anthony Russo, Joe Russo",
        actors: "Robert Downey Jr., Chris Evans, Mark Ruffalo, Chris Hemsworth",
        description:
          "After the devastating events of Avengers: Infinity War (2018), the universe is in ruins. With the help of remaining allies, the Avengers assemble once more in order to reverse Thanos' actions and restore balance to the universe.",
        asset: "./../assets/endgame.jpg",
        belongsToMyCollection: true,
      },
    ];
    await prepareAddOfMovie(addedMovie);

    const expectedMovies = [
      {
        _id: "1",
        name: "Avengers: Endgame",
        year: 2019,
        genre: "Action, Adventure, Drama",
        director: "Anthony Russo, Joe Russo",
        actors: "Robert Downey Jr., Chris Evans, Mark Ruffalo, Chris Hemsworth",
        description:
          "After the devastating events of Avengers: Infinity War (2018), the universe is in ruins. With the help of remaining allies, the Avengers assemble once more in order to reverse Thanos' actions and restore balance to the universe.",
        asset: "./../assets/endgame.jpg",
        belongsToMyCollection: true,
      },
    ];
    checkMovieList(expectedMovies);
  });

  test("change collection of movie successfully", async () => {
    // given
    // 1. Vorbereiten von Änderung von Angebot
    //    (und Laden von Angeboten)
    const changedMovie = {
      _id: "1",
      name: "Avengers: Endgame",
      year: 2019,
      genre: "Action, Adventure, Drama",
      director: "Anthony Russo, Joe Russo",
      actors: "Robert Downey Jr., Chris Evans, Mark Ruffalo, Chris Hemsworth",
      description:
        "After the devastating events of Avengers: Infinity War (2018), the universe is in ruins. With the help of remaining allies, the Avengers assemble once more in order to reverse Thanos' actions and restore balance to the universe.",
      asset: "./../assets/endgame.jpg",
      belongsToMyCollection: false,
    };
    const moviesToLoad = [
      {
        _id: "1",
        name: "Avengers: Endgame",
        year: 2019,
        genre: "Action, Adventure, Drama",
        director: "Anthony Russo, Joe Russo",
        actors: "Robert Downey Jr., Chris Evans, Mark Ruffalo, Chris Hemsworth",
        description:
          "After the devastating events of Avengers: Infinity War (2018), the universe is in ruins. With the help of remaining allies, the Avengers assemble once more in order to reverse Thanos' actions and restore balance to the universe.",
        asset: "./../assets/endgame.jpg",
        belongsToMyCollection: true,
      },
      {
        _id: "2",
        name: "Creed II",
        year: 2018,
        genre: "Drama, Sport",
        director: "Steven Caple Jr.",
        actors:
          "Michael B. Jordan, Sylvester Stallone, Tessa Thompson, Phylicia Rashad",
        description:
          "Under the tutelage of Rocky Balboa, newly crowned heavyweight champion Adonis Creed faces off against Viktor Drago, the son of Ivan Drago.",
        asset: "./../assets/creed2.jpg",
        belongsToMyCollection: true,
      },
      {
        _id: "3",
        name: "Grown Ups 2",
        year: 2013,
        genre: "Comedy",
        director: "Dennis Dugan",
        actors: "Adam Sandler, Kevin James, Chris Rock, David Spade",
        description:
          "After moving his family back to his hometown to be with his friends and their kids, Lenny finds out that between old bullies, new bullies, schizo bus drivers, drunk cops on skis, and four hundred costumed party crashers sometimes crazy follows you.",
        asset: "./../assets/grownups2.jpg",
        belongsToMyCollection: true,
      },
    ];
    await prepareChangeOfMovie(changedMovie, moviesToLoad);
    // 2. Angebot auswählen und neuen Preis Eingabefeld hinzufügen

    const expectedMovies = [
      {
        _id: "1",
        name: "Avengers: Endgame",
        year: 2019,
        genre: "Action, Adventure, Drama",
        director: "Anthony Russo, Joe Russo",
        actors: "Robert Downey Jr., Chris Evans, Mark Ruffalo, Chris Hemsworth",
        description:
          "After the devastating events of Avengers: Infinity War (2018), the universe is in ruins. With the help of remaining allies, the Avengers assemble once more in order to reverse Thanos' actions and restore balance to the universe.",
        asset: "./../assets/endgame.jpg",
        belongsToMyCollection: false,
      },
      {
        _id: "2",
        name: "Creed II",
        year: 2018,
        genre: "Drama, Sport",
        director: "Steven Caple Jr.",
        actors:
          "Michael B. Jordan, Sylvester Stallone, Tessa Thompson, Phylicia Rashad",
        description:
          "Under the tutelage of Rocky Balboa, newly crowned heavyweight champion Adonis Creed faces off against Viktor Drago, the son of Ivan Drago.",
        asset: "./../assets/creed2.jpg",
        belongsToMyCollection: true,
      },
      {
        _id: "3",
        name: "Grown Ups 2",
        year: 2013,
        genre: "Comedy",
        director: "Dennis Dugan",
        actors: "Adam Sandler, Kevin James, Chris Rock, David Spade",
        description:
          "After moving his family back to his hometown to be with his friends and their kids, Lenny finds out that between old bullies, new bullies, schizo bus drivers, drunk cops on skis, and four hundred costumed party crashers sometimes crazy follows you.",
        asset: "./../assets/grownups2.jpg",
        belongsToMyCollection: true,
      },
    ];
    checkMovieList(expectedMovies);
  });
});

describe("loading Popup correctly", () => {
  beforeEach(() => {
    setUpDom();
  });

  afterEach(() => {
    jest.resetModules();
    jest.resetAllMocks();
  });
  test("movie details are displayed when a movie is clicked", async () => {
    // given
    const loadedMovie = [
      {
        _id: "1",
        name: "Avengers: Endgame",
        year: 2019,
        genre: "Action, Adventure, Drama",
        director: "Anthony Russo, Joe Russo",
        actors: "Robert Downey Jr., Chris Evans, Mark Ruffalo, Chris Hemsworth",
        description:
          "After the devastating events of Avengers: Infinity War (2018), the universe is in ruins. With the help of remaining allies, the Avengers assemble once more in order to reverse Thanos' actions and restore balance to the universe.",
        asset: "./../assets/endgame.jpg",
        belongsToMyCollection: true,
      },
    ];
    await prepareLoadOfMovies(loadedMovie);
    // then

    // when
    console.log(document.querySelector("#data"));
    const movieElement = document.querySelector("#movie" + loadedMovie[0]._id);
    movieElement.click();
    // then
    const displayedName = document.querySelector(`#moviename`).textContent;

    const displayedYear = document.querySelector("#movieyear").textContent;

    const elPopup = document.querySelector("#popup");
    expect(elPopup.classList.contains("hidden")).toBe(false);
    expect(displayedName).toBe(loadedMovie[0].name);
    expect(displayedYear).toBe("Year: " + loadedMovie[0].year);
  });
  test("movie details are displayed when a movie is clicked and unloaded again once closed", async () => {
    // given
    const loadedMovie = [
      {
        _id: "1",
        name: "Avengers: Endgame",
        year: 2019,
        genre: "Action, Adventure, Drama",
        director: "Anthony Russo, Joe Russo",
        actors: "Robert Downey Jr., Chris Evans, Mark Ruffalo, Chris Hemsworth",
        description:
          "After the devastating events of Avengers: Infinity War (2018), the universe is in ruins. With the help of remaining allies, the Avengers assemble once more in order to reverse Thanos' actions and restore balance to the universe.",
        asset: "./../assets/endgame.jpg",
        belongsToMyCollection: true,
      },
    ];
    await prepareLoadOfMovies(loadedMovie);
    // then

    // when
    console.log(document.querySelector("#data"));
    const movieElement = document.querySelector("#movie" + loadedMovie[0]._id);
    movieElement.click();
    // then
    const displayedName = document.querySelector(`#moviename`).textContent;

    const displayedYear = document.querySelector("#movieyear").textContent;

    const elPopup = document.querySelector("#popup");
    expect(elPopup.classList.contains("hidden")).toBe(false);
    expect(displayedName).toBe(loadedMovie[0].name);
    expect(displayedYear).toBe("Year: " + loadedMovie[0].year);

    const elClose = document.getElementById("close");
    elClose.click();
    expect(elPopup.classList.contains("hidden")).toBe(true);
  });
});

describe("Adding Movie successfully to Collection", () => {
  beforeEach(() => {
    setUpDom();
  });

  afterEach(() => {
    jest.resetModules();
    jest.resetAllMocks();
  });

  function checkMovieList(expectedMovies) {
    const elDivMovies = document.getElementsByClassName("movie");

    // check number of rows in offerings table
    expect(elDivMovies.length).toBe(expectedMovies.length);

    for (let i = 0; i < elDivMovies.length; i++) {
      const elDivMovie = elDivMovies[i];

      const expectedMovie = expectedMovies[i];
      expect(
        elDivMovie.classList.contains(expectedMovie.belongsToMyCollection)
      ).toBe(expectedMovie.belongsToMyCollection);
    }
  }

  function prepareChangeOfMovie(changedMovie, loadedMovies) {
    axiosModule.put.mockResolvedValue({ data: changedMovie });
    return initMain(loadedMovies);
  }
  test("Movie correctly added to Collection", async () => {
    // given
    const loadedMovie = [
      {
        _id: "1",
        name: "Avengers: Endgame",
        year: 2019,
        genre: "Action, Adventure, Drama",
        director: "Anthony Russo, Joe Russo",
        actors: "Robert Downey Jr., Chris Evans, Mark Ruffalo, Chris Hemsworth",
        description:
          "After the devastating events of Avengers: Infinity War (2018), the universe is in ruins. With the help of remaining allies, the Avengers assemble once more in order to reverse Thanos' actions and restore balance to the universe.",
        asset: "./../assets/endgame.jpg",
        belongsToMyCollection: false,
      },
    ];
    await prepareLoadOfMovies(loadedMovie);
    // then

    // when
    console.log(document.querySelector("#data"));
    const movieElement = document.querySelector("#movie" + loadedMovie[0]._id);
    movieElement.click();
    // then
    const displayedName = document.querySelector(`#moviename`).textContent;

    const displayedYear = document.querySelector("#movieyear").textContent;

    const elPopup = document.querySelector("#popup");
    expect(elPopup.classList.contains("hidden")).toBe(false);
    expect(displayedName).toBe(loadedMovie[0].name);
    expect(displayedYear).toBe("Year: " + loadedMovie[0].year);

    var event = new KeyboardEvent("keypress", { keycode: 72 });
    document.dispatchEvent(event);

    const updatedMovie = [
      {
        _id: "1",
        name: "Avengers: Endgame",
        year: 2019,
        genre: "Action, Adventure, Drama",
        director: "Anthony Russo, Joe Russo",
        actors: "Robert Downey Jr., Chris Evans, Mark Ruffalo, Chris Hemsworth",
        description:
          "After the devastating events of Avengers: Infinity War (2018), the universe is in ruins. With the help of remaining allies, the Avengers assemble once more in order to reverse Thanos' actions and restore balance to the universe.",
        asset: "./../assets/endgame.jpg",
        belongsToMyCollection: true,
      },
    ];

    await prepareChangeOfMovie(updatedMovie, loadedMovie);

    checkMovieList(updatedMovie);

    expect(elPopup.classList.contains("hidden")).toBe(true);
  });
});
