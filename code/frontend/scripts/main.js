import movieService from "./movieService.js";

const wholeMovieList = [];
let myMovies = [];
let newMovies = [];
let needMoviesToBeLoaded = false;

function loadMovies() {
  myMovies = [];
  newMovies = [];
  let myMoviesCycle = 0;
  let newMoviesCycle = 0;
  movieService
    .getAll()
    .then((response) => {
      const movies = response;
      const elDivMovies = document.getElementById("data");
      elDivMovies.innerHTML = "";
      if (wholeMovieList.length < 1) {
        needMoviesToBeLoaded = true;
      } else {
        needMoviesToBeLoaded = false;
      }
      for (let i = 0; i < movies.length; i++) {
        if (needMoviesToBeLoaded) {
          wholeMovieList.push(movies[i]);
        }
      }
      const queryString = window.location.search;
      const urlParams = new URLSearchParams(queryString);
      if (urlParams.has("belongsToMyCollection")) {
        switch (urlParams.get("belongsToMyCollection")) {
          case "true":
            const myMoviesFiltered = movies.filter(
              (movies) => movies.belongsToMyCollection == true
            );
            for (let i = 0; i < myMoviesFiltered.length; i++) {
              myMoviesCycle += 1;
              myMovies.push(myMoviesFiltered[i]);
              const elDiv = document.createElement("div");
              elDiv.setAttribute("id", "movie" + myMoviesCycle);
              elDiv.classList.add("movie");
              const elH3 = document.createElement("h3");
              elH3.classList.add("name");
              elH3.textContent = myMoviesFiltered[i].name;
              elH3.setAttribute("id", "name" + (i + 1));
              elH3.setAttribute("title", myMoviesFiltered[i].name);
              const elImg = document.createElement("img");
              elImg.src = myMoviesFiltered[i].asset;
              elImg.setAttribute("id", "img" + (i + 1));

              // const elP = document.createElement("p");
              // elP.textContent = movie.description;
              elDiv.appendChild(elH3);
              elDiv.appendChild(elImg);
              // elDiv.appendChild(elP);
              elDiv.addEventListener("click", () => {
                previousKey = i + 1;
                loadDataIntoPopup(myMoviesFiltered, i + 1);
              });
              elDivMovies.appendChild(elDiv);
            }
            break;
          case "false":
            const newMoviesFiltered = movies.filter(
              (movies) => movies.belongsToMyCollection == false
            );
            for (let i = 0; i < newMoviesFiltered.length; i++) {
              newMoviesCycle += 1;
              newMovies.push(newMoviesFiltered[i]);
              const elDiv = document.createElement("div");
              elDiv.setAttribute("id", "movie" + newMoviesCycle);
              elDiv.classList.add("movie");
              const elH3 = document.createElement("h3");
              elH3.classList.add("name");
              elH3.textContent = newMoviesFiltered[i].name;
              elH3.setAttribute("id", "name" + (i + 1));
              elH3.setAttribute("title", newMoviesFiltered[i].name);
              const elImg = document.createElement("img");
              elImg.src = newMoviesFiltered[i].asset;
              elImg.setAttribute("id", "img" + (i + 1));

              // const elP = document.createElement("p");
              // elP.textContent = movie.description;
              elDiv.appendChild(elH3);
              elDiv.appendChild(elImg);
              // elDiv.appendChild(elP);
              elDiv.addEventListener("click", () => {
                previousKey = i + 1;
                loadDataIntoPopup(newMoviesFiltered, i + 1);
              });
              elDivMovies.appendChild(elDiv);
            }
            break;
        }
      } else {
        loadMoviesWithFilter(wholeMovieList);
      }
    })
    .catch((error) => {
      console.error("Error loading movies:", error);
      const networkError = new Error("Failed to load Movies");
      networkError.statusCode = 500;
      throw networkError;
    });
}

function addMovie(NrOfMovie) {
  const movie = newMovies[NrOfMovie - 1];
  movie.belongsToMyCollection = true;
  const movieId = movie._id;
  movie.id = movieId;
  movieService
    .update(movieId, movie)
    .then((response) => {
      loadMovies();
    })
    .catch((error) => {
      const networkError = new Error("Failed to add Movie");
      networkError.statusCode = 500;
      throw networkError;
    });
  const elPopup = document.getElementById("popup");
  elPopup.classList.add("hidden");
}
function removeMovie(NrOfMovie) {
  const movie = myMovies[NrOfMovie - 1];
  movie.belongsToMyCollection = false;
  const movieId = movie._id;
  movie.id = movieId;
  movieService
    .update(movieId, movie)
    .then((response) => {
      loadMovies();
    })
    .catch((error) => {
      const networkError = new Error("Failed to remove Movie");
      networkError.statusCode = 500;
      throw networkError;
    });
  const elPopup = document.getElementById("popup");
  elPopup.classList.add("hidden");
}

function loadMoviesWithFilter(filteredArray) {
  const movies = filteredArray;
  const elDivMovies = document.getElementById("data");
  elDivMovies.innerHTML = "";
  let currentCycle = 0;

  for (let i = 0; i < movies.length; i++) {
    currentCycle++;

    const elDiv = document.createElement("div");
    elDiv.classList.add(movies[i].belongsToMyCollection);
    elDiv.setAttribute("id", "movie" + currentCycle);
    elDiv.classList.add("movie");
    const elH3 = document.createElement("h3");
    elH3.classList.add("name");
    elH3.textContent = movies[i].name;
    elH3.setAttribute("id", "name" + (i + 1));
    const elImg = document.createElement("img");
    elImg.src = movies[i].asset;
    elImg.setAttribute("id", "img" + (i + 1));

    // const elP = document.createElement("p");
    // elP.textContent = movie.description;
    elDiv.appendChild(elH3);
    elDiv.appendChild(elImg);
    // elDiv.appendChild(elP);
    elDiv.addEventListener("click", () => {
      previousKey = i + 1;
      loadDataIntoPopup(filteredArray, i + 1);
    });
    elDivMovies.appendChild(elDiv);
  }
}
function searchMovie() {
  const searchInput = document.getElementById("search-input").value;

  let query = searchInput;
  query = query.toLowerCase();
  const queryString = window.location.search;
  const urlParams = new URLSearchParams(queryString);
  if (urlParams.has("belongsToMyCollection")) {
    switch (urlParams.get("belongsToMyCollection")) {
      case "true":
        if (query == "") {
          loadMovies();
        } else {
          const searchedMyMovies = myMovies.filter((movie) => {
            let name = movie.name;
            name = name.toLowerCase();
            return name.includes(query);
          });
          loadMoviesWithFilter(searchedMyMovies);
        }
        break;
      case "false":
        if (query == "") {
          loadMovies();
        } else {
          const searchedNewMovies = newMovies.filter((movie) => {
            let name = movie.name;
            name = name.toLowerCase();
            return name.includes(query);
          });
          loadMoviesWithFilter(searchedNewMovies);
        }
        break;
    }
  }
}

let previousKey = 0;

function loadDataIntoPopup(useThisArray, pressedKey) {
  const elPopup = document.getElementById("popup");
  const elXDiv = document.createElement("div");
  const elInfo = document.createElement("div");
  const elTitle = document.createElement("h1");
  const elYear = document.createElement("h2");
  const elGenre = document.createElement("h2");
  const elDirector = document.createElement("h2");
  const elActors = document.createElement("h2");
  const elDiscription = document.createElement("p");
  const elImg = document.createElement("img");
  elPopup.innerHTML = "";
  elInfo.classList.add("info-popup");
  elTitle.textContent = useThisArray[pressedKey - 1].name;
  elTitle.setAttribute("id", "moviename");
  elYear.textContent = "Year: " + useThisArray[pressedKey - 1].year;
  elYear.setAttribute("id", "movieyear");
  elGenre.textContent = "Genre: " + useThisArray[pressedKey - 1].genre;
  elDirector.textContent =
    "Directors: " + useThisArray[pressedKey - 1].director;
  elActors.textContent = "Actors: " + useThisArray[pressedKey - 1].actors;
  elDiscription.textContent = useThisArray[pressedKey - 1].description;
  elImg.src = useThisArray[pressedKey - 1].asset;
  elImg.classList.add("img-pop");
  elXDiv.textContent = "x";
  elXDiv.classList.add("x-div");
  elXDiv.setAttribute("id", "close");
  elXDiv.addEventListener("click", () => {
    elPopup.classList.add("hidden");
  });

  elPopup.appendChild(elXDiv);
  elPopup.appendChild(elImg);
  elInfo.appendChild(elTitle);
  elInfo.appendChild(elYear);
  elInfo.appendChild(elGenre);
  elInfo.appendChild(elDirector);
  elInfo.appendChild(elActors);
  elPopup.appendChild(elInfo);
  elPopup.appendChild(elDiscription);

  elPopup.classList.remove("hidden");
}

function addEventMyCollection(pressedKey) {
  if (
    (pressedKey >= 1 && pressedKey <= myMovies.length) ||
    pressedKey === "Escape"
  ) {
    if (pressedKey === "Escape") {
      const elPreviousMovie = document.getElementById("movie" + previousKey);
      const elPopup = document.getElementById("popup");
      if (elPreviousMovie != null) {
        elPopup.classList.add("hidden");
        elPreviousMovie.classList.remove("selected");
        previousKey = 0;
      }
    } else {
      if (
        previousKey >= 1 &&
        previousKey <= myMovies.length &&
        previousKey !== pressedKey
      ) {
        const elPreviousMovie = document.getElementById("movie" + previousKey);
        elPreviousMovie.classList.remove("selected");
      }
      const elDivMovie = document.getElementById("movie" + pressedKey);
      if (elDivMovie != null) {
        // Check if elDivMovie is not null
        if (elDivMovie.classList.contains("selected")) {
          elDivMovie.classList.remove("selected");
          previousKey = 0;
        } else {
          elDivMovie.classList.add("selected");
          previousKey = pressedKey;
        }
      }
      loadDataIntoPopup(myMovies, pressedKey);
    }
  }
}

function addEventNewMovies(pressedKey) {
  if (
    (pressedKey >= 1 && pressedKey <= newMovies.length) ||
    pressedKey === "Escape"
  ) {
    if (pressedKey === "Escape") {
      const elPreviousMovie = document.getElementById("movie" + previousKey);
      const elPopup = document.getElementById("popup");
      if (elPreviousMovie != null) {
        elPopup.classList.add("hidden");
        elPreviousMovie.classList.remove("selected");
        previousKey = 0;
      }
    } else {
      if (
        previousKey >= 1 &&
        previousKey <= newMovies.length &&
        previousKey !== pressedKey
      ) {
        const elPreviousMovie = document.getElementById("movie" + previousKey);
        elPreviousMovie.classList.remove("selected");
      }
      const elDivMovie = document.getElementById("movie" + pressedKey);
      if (elDivMovie != null) {
        // Check if elDivMovie is not null
        if (elDivMovie.classList.contains("selected")) {
          elDivMovie.classList.remove("selected");
          previousKey = 0;
        } else {
          elDivMovie.classList.add("selected");
          previousKey = pressedKey;
        }
      }

      loadDataIntoPopup(newMovies, pressedKey);
    }
  }
}

document.addEventListener("keyup", (e) => {
  const queryString = window.location.search;
  const urlParams = new URLSearchParams(queryString);
  const pressedKey = e.key;

  // is the pressed key a number between 1 and 4?

  if (urlParams.has("belongsToMyCollection")) {
    switch (urlParams.get("belongsToMyCollection")) {
      case "true":
        addEventMyCollection(pressedKey);

        break;
      case "false":
        console.log(newMovies);

        addEventNewMovies(pressedKey);

        break;
    }
  }

  if (pressedKey == "h") {
    if (urlParams.has("belongsToMyCollection")) {
      if (urlParams.get("belongsToMyCollection") == "false") {
        addMovie(previousKey);
      }
    } else {
      addMovie(previousKey);
    }
  }
  if (pressedKey == "l") {
    if (urlParams.has("belongsToMyCollection")) {
      if (urlParams.get("belongsToMyCollection") == "true") {
        removeMovie(previousKey);
      }
    } else {
      removeMovie(previousKey);
    }
  }
});

function moviesRedirect(location) {
  window.location.href = `http://localhost:3001/?belongsToMyCollection=${location}`;
}

function addButtonListener() {
  const elMyMoviesBtn = document.getElementById("my-movies-btn");
  elMyMoviesBtn.addEventListener("click", () => {
    moviesRedirect(true);
  });
  const elNewMoviesBtn = document.getElementById("new-movies-btn");
  elNewMoviesBtn.addEventListener("click", () => {
    moviesRedirect(false);
  });
}

const elSearch = document.getElementById("search-btn");

elSearch.addEventListener("click", searchMovie);

window.onload = function () {};
loadMovies();

addButtonListener();
