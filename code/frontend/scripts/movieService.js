const baseUrl = "/api/movie";

const getAll = () => {
  const request = axios.get(baseUrl);
  return handleResponse(request);
};

const create = (newObject) => {
  const request = axios.post(baseUrl, newObject);
  return handleResponse(request);
};

const update = (id, newObject) => {
  const request = axios.put(`${baseUrl}/${id}`, newObject);
  return handleResponse(request);
};

const remove = (id) => {
  const request = axios.delete(`${baseUrl}/${id}`);
  return handleResponse(request);
};

const handleResponse = (request) => {
  return request
    .then((response) => {
      return response.data;
    })
    .catch((error) => {
      return [];
    });
};

export default { getAll, create, update, remove };
